import pandas as pd
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist
import numpy as np
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import DBSCAN
from sklearn.mixture import GaussianMixture


FILE_PATH = "../data/bacteria.txt"


class MyKMeans:
    """
    My KMeans class
    """
    def __init__(self, k, max_iteration=100, progress_callback=None):
        self.k = k
        self.max_iteration = max_iteration
        self._clusters_centroids = np.array([])
        self._progress_callback = progress_callback

    def fit(self, X, y=None):
        self._clusters_centroids = np.array([np.array(X[i, :]) for i in range(self.k)])

        for i in range(self.max_iteration):
            self._run_iteration(X)
            # callback
            if self._progress_callback is not None:
                self._progress_callback(X, self.cluster_ids, self._clusters_centroids)

        print(self._clusters_centroids)

    def _run_iteration(self, X):
        dist_from_centroids = cdist(X, self._clusters_centroids)
        self.cluster_ids = np.argmin(dist_from_centroids, axis=1)
        self._clusters_centroids = np.array([X[self.cluster_ids == c, :].mean(axis=0) for c in range(self.k)])
        pass

    def predict(self, X):
        if not np.any(self._clusters_centroids):
            print("Run fit first!!")
        dist_from_centroids = cdist(np.array(X), self._clusters_centroids)
        cluster_ids = np.argmin(dist_from_centroids, axis=1)
        return cluster_ids

    def fit_predict(self, X, y=np.array([])):
        self.fit(X, y)
        return self.predict(X)


def plot_elbow_for_kmeans(X):
    # k means determine k
    msd = []
    K = range(1, 10)
    for k in K:
        kmeanModel = KMeans(n_clusters=k).fit(X)
        msd.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_), axis=1)) / X.shape[0])

    # Plot the elbow
    fig = plt.figure()
    plt.plot(K, msd, 'bx-')
    plt.xlabel('K')
    plt.ylabel('Mean Squared Distance')
    plt.title('Elbow Curve')
    fig.show()


def plot_data(df_bacteria, title, clusters=None, cmap=None):
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    plt.title('Bacteria Colony')
    if np.any(clusters):
        scat = ax.scatter(df_bacteria[:, 0], df_bacteria[:, 1], c=clusters, cmap=cmap)
    else:
        scat = ax.scatter(df_bacteria[:, 0], df_bacteria[:, 1])
    cb = plt.colorbar(scat, spacing='proportional')
    cb.set_label('clusters')
    ax.set_title(title)
    fig.show()


def main():
    k = 4
    bacteria_df = pd.read_csv(FILE_PATH)
    bacteria = bacteria_df.to_numpy()
    # a - plot the data
    plot_data(bacteria, "Bacteria Data")

    # K-Means implementation
    my_kmeans = MyKMeans(k, max_iteration=20)
    my_kmeans.fit(bacteria)
    clusters = my_kmeans.predict(bacteria)

    # define the colormap
    cmap = plt.cm.jet
    # extract all colors from the .jet map
    cmaplist = [cmap(i) for i in range(cmap.N)]
    # create the new map
    cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)

    plot_data(bacteria, "My K-Means", clusters, cmap)

    plot_elbow_for_kmeans(bacteria)

    # pythons libraries
    # 1. Kmeans
    km_model = KMeans(4)
    km_model.fit(bacteria)
    km_predictions = km_model.predict(bacteria)
    plot_data(bacteria, "Python K-means", km_predictions, cmap)

    # 2. HAC
    hac_model = AgglomerativeClustering(n_clusters=7, affinity='euclidean', linkage='ward')
    hac_predictions = hac_model.fit_predict(bacteria)
    plot_data(bacteria, "Python HAC", hac_predictions, cmap)

    # 3. DBSCAN
    dbscan_model = DBSCAN(eps=1.8, min_samples=12)
    dbscan_predictions = dbscan_model.fit_predict(bacteria)
    plot_data(bacteria, "Python DBSCAN", dbscan_predictions, cmap)

    # 4. GMM
    gmm = GaussianMixture(n_components=6)
    gmm_predictions = gmm.fit_predict(bacteria)
    plot_data(bacteria, "Python GMM", gmm_predictions, cmap)
    plt.show()


if __name__ == '__main__':
    main()
